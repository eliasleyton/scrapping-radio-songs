var extract	= require('./extract.js');
var model  	= require('./models');
var song 	= model.Song;

	song.sync();

	extract.viraliza('Niu', 'https://realtime.us.viraliza.la/nowplaying/api/5537245999402c00e34246a6/InitialNowPlaying.json', function (data) {
		
		var d = {
			radio: data.radio,
			title: data.title,
			artist: data.artist
		};

		song.create(d).then(function () {
			console.log('New Song: [%s] %s - %s ', data.radio, data.artist, data.title);
		});
	});
