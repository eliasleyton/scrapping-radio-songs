var express = require('express');
var cors = require('cors');

var app = express();

app.use(cors());

app.get('/chart', function (req, res) {
  

	var n, i, nd;

	n = Math.floor((Math.random() * 1000) + 1);
	i = Math.floor((Math.random() * 1000) + 1);
	nd = Math.floor((Math.random() * 1000) + 1);

	data = {
		national: n,
		international: i,
		not_defined: nd
	};


  res.json(data);
});

var server = app.listen(8088, function () {
  var host = server.address().address;
  var port = server.address().port;

  console.log('Listening at http://%s:%s', host, port);
});