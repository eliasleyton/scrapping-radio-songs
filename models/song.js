"use strict";

module.exports = function (Sequelize, DataTypes) {
    return Sequelize.define('Song', {
    	radio: { type: DataTypes.STRING },
        title: { type: DataTypes.STRING },
        artist: { type: DataTypes.TEXT },
    });
};