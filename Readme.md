#Titulo

Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

```
KEY = 3II1DASCGN61IL6AX 
```

###Notas

Buscar Artista y obtener artist id
```
http://developer.echonest.com/api/v4/artist/search?api_key=CUSTOMER_KEY&name=ARTIST_NAME
```

Obtiene pais del artistas
```
http://developer.echonest.com/api/v4/artist/profile?api_key=CUSTOMER_KEY&id=ARTIST_ID&bucket=artist_location
```

##Radios



			http://players.grupodial.cl/player/json/zero/last.json



Duna 88.9 (Grupo dial, digitalproserver.com)  // Quefeoxml

```
http://vivo.duna.cl/stats/last10.xml
``` 

Radio Horizonte (.cl) (Media Stream)

```
http://nowplaying.s-mdstrm.com/Nowplaying/cache/history_5124f1b4ed596bde7d000017.json
```

Injuv (Single historial)

```
http://injuv.digitalproserver.com/json/now.json
```

Niu Radio CL (viraliza.la)

```
https://realtime.us.viraliza.la/nowplaying/api/5537245999402c00e34246a6/InitialNowPlaying.json
```

Radio Rinoceronte (.cl) (Media Stream)

```
http://nowplaying.s-mdstrm.com/Nowplaying/cache/history_52f95ba874f267492a000041.json
```

Imagina - Prisa/Union Radio (triton)

```
http://np.tritondigital.com/public/nowplaying?mountName=IMAGINA&numberToFetch=10&eventType=track
```