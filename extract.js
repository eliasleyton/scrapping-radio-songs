var request = require('request');
var toJSON 	= require('xml2js').parseString;
var cheerio = require('cheerio');

module.exports = {
	dps: function(radio, ws, callback) {
		request(ws +'?'+ Math.random(), function (error, response, data) {
			if (!error && response.statusCode == 200) {

				toJSON(data, function(err, result) {

					$ = cheerio.load(result.last.track[0].cancion.toString());

					var ar = $('a').text().split('-');

					var artist, title, country;

					if (ar.length === 2) {
						artist = ar[0].trim();
						title = ar[1].trim();
					} else {
						callback(null);
					}

					callback({
						radio: radio,
						artist: artist,
						title: title,
					});
				});
			} else {
				callback(error);
			}
		});
	},
	dps_: function(radio, ws, callback) {

		// custom Injuv JSON
		// trannks @alvaroveliz
		
		request(ws +'?'+ Math.random(), function (error, response, data) {
			if (!error && response.statusCode == 200) {	
				eval(data);
			} else {
				callback(error);
			}
		});
		
		var now = function (data) {
			data = data.items.shift();
			callback({
				radio: radio,
				artist: data.artist,
				title: data.song
			});
		}
	},
	mediaStream: function (radio, ws, callback) {

		request(ws +'?'+ Math.random(), function (error, response, data) {

			if (!error && response.statusCode == 200) {

				data = JSON.parse(data);

				callback({
					radio: radio,
					artist: data.data[0].artist_name,
					title: data.data[0].song_title
				});

			} else {
				callback(error);
			}
		});
	},
	viraliza: function (radio, ws, callback) {

		request(ws +'?'+ Math.random(), function (error, response, data) {

			if (!error && response.statusCode == 200) {

				data = JSON.parse(data);

				callback({
					radio: radio,
					artist: data.nowplaying.artist,
					title: data.nowplaying.track
				});

			} else {
				callback(error);
			}
		});
	},
	triton: function (radio, ws, callback) { 

		request(ws, function (error, response, data) {
		
			toJSON(data, function(err, result) { 
				var a = result["nowplaying-info-list"]["nowplaying-info"].shift().property;

				callback({
					radio: radio,
					title: a[1]._,
					artist: a[3]._
				});

			});

		});
	},
	getCountry: function (artist, callback) {
		
		var echonest_api_key = 'YOU_API_KEY';
		
		var echonest_url = 'http://developer.echonest.com/api/v4/artist/profile?';

		echonest_url += 'name=' + artist;
		echonest_url += '&api_key=' + echonest_api_key + '&bucket=artist_location';

		request(echonest_url, function(e, res, data) {
			data = JSON.parse(data);
			try {
				country = data.response.artist.artist_location.country;
			} catch (err) {
				callback(null);
			}
			return callback(country);
		});
	}
}